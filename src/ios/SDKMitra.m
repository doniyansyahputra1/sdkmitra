/********* SDKMitra.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <Foundation/Foundation.h>
#import "IncomingViewController.h"
#import "VideoViewController.h"
#import "SDKMitra.h"


@interface SDKMitra ()
{
  BOOL        sipRegistered;
  SoundService* _mSoundService;
  Reachability* internetReach;
  IncomingViewController* incomingViewController;
  long _activeSessionId;
  long _lineSessions[MAX_LINES];
    
    NSString *_VoIPPushToken;
    UIBackgroundTaskIdentifier _backtaskIdentifier;

}

@end

@implementation SDKMitra

+(SDKMitra*) sharedInstance{
    return ((SDKMitra*) [[UIApplication sharedApplication] delegate]);
}


- (int)findSession:(long)sessionId
{
    for(int i = 0; i < MAX_LINES; i++)
    {
        if(_lineSessions[i] == sessionId)
        {
            return i;
        }
    }
    NSLog(@"Can't find session, Not exist this SessionId = %ld", sessionId);
    return -1;
};

- (int)findIdleLine
{
    for(int i = 0; i < MAX_LINES; i++)
    {
        if(_lineSessions[i] == INVALID_SESSION_ID)
        {
            return i;
        }
    }
    NSLog(@"No idle line available. All lines are in use.");
    return -1;
};

- (void)freeLine:(long)sessionId
{
    for(int i = 0; i < MAX_LINES; i++)
    {
        if(_lineSessions[i] == sessionId)
        {
            _lineSessions[i] = INVALID_SESSION_ID;
            return ;
        }
    }
    NSLog(@"Can't Free Line, Not exist this SessionId = %ld", sessionId);
};


- (void) pressNumpadButton:(char )dtmf
{
    if(_activeSessionId != INVALID_SESSION_ID)
    {
        [_callManager playDtmf:_activeSessionId tone:dtmf];
    }
}

- (long) makeCall:(NSString*) callee
        videoCall:(BOOL)videoCall
{
    if(_activeSessionId != INVALID_SESSION_ID )
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Warning"
                              message: @"Current line is busy now, please switch a line"
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles:nil];
        [alert show];
        
        return INVALID_SESSION_ID;
    }
    
    
    long newSessionId  = [_callManager makeCall:callee displayName:callee videoCall:videoCall];
    if(newSessionId >= 0)
    {
        //[loginViewController setStatusText:[NSString  stringWithFormat:@"Calling:%@ on line %zd", callee, _activeLine]];
        _activeSessionId = newSessionId;
        return _activeSessionId;
    }
    else{
        // [loginViewController setStatusText:[NSString  stringWithFormat:@"make call failure ErrorCode =%zd", newSessionId]];
        return newSessionId;
    }
}

\

- (void)EndView
{
    
}



- (void) hungUpCall
{
    if(_activeSessionId != INVALID_SESSION_ID )
    {
        [_mSoundService stopRingTone];
        [_mSoundService stopRingBackTone];
        
        [_callManager endCall:_activeSessionId];
        //[self EndView];
        
    }
}

- (void)holdCall
{
    if(_activeSessionId != INVALID_SESSION_ID )
    {
        [_callManager holdCall:_activeSessionId onHold:YES];
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"hold call on line %zd", _activeLine]];
    
    if (_isConference) {
        [_callManager holdAllCall:YES];
    }
}

- (void) unholdCall
{
    if(_activeSessionId != INVALID_SESSION_ID )
    {
        [_callManager holdCall:_activeSessionId onHold:NO];
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"UnHold the call on line %zd", _activeLine]];
    
    if (_isConference) {
        [_callManager holdAllCall:NO];
    }
    
}

- (void) referCall:(NSString*)referTo
{
    HSSession* session = [_callManager findCallBySessionID:_activeSessionId];
    if (session == nil || !session.sessionState)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Warning"
                              message: @"Need to make the call established first"
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    int errorCodec = [portSIPSDK refer:_activeSessionId referTo:referTo];
    if (errorCodec != 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Warning"
                              message: @"Refer failed"
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) getStatistics
{
    {
        //audio Statistics
        int  sendBytes; int  sendPackets;
        int  sendPacketsLost; int  sendFractionLost;
        int  sendRttMS;
        int  sendCodecType;
        int  sendJitterMS; int  sendAudioLevel;
        int  recvBytes; int  recvPackets;
        int  recvPacketsLost; int  recvFractionLost;
        int  recvCodecType;int  recvJitterMS;
        int  recvAudioLevel;
        
        int errorCodec = [portSIPSDK getAudioStatistics:_activeSessionId
                                              sendBytes:&sendBytes
                                            sendPackets:&sendPackets
                                        sendPacketsLost:&sendPacketsLost
                                       sendFractionLost:&sendFractionLost
                                              sendRttMS:&sendRttMS
                                          sendCodecType:&sendCodecType
                                           sendJitterMS:&sendJitterMS
                                         sendAudioLevel:&sendAudioLevel
                                              recvBytes:&recvBytes
                                            recvPackets:&recvPackets
                                        recvPacketsLost:&recvPacketsLost
                                       recvFractionLost:&recvFractionLost
                                          recvCodecType:&recvCodecType
                                           recvJitterMS:&recvJitterMS
                                         recvAudioLevel:&recvAudioLevel];
        
        if(errorCodec == 0)
        {
            NSLog(@"Audio Send Statistics sendBytes:%d sendPackets:%d sendPacketsLost:%d sendFractionLost:%d sendRttMS:%d sendCodecType:%d sendJitterMS:%d sendAudioLevel:%d ",sendBytes,sendPackets,sendPacketsLost,sendFractionLost,sendRttMS,sendCodecType,sendJitterMS,sendAudioLevel);
            NSLog(@"Audio Received Statistics recvBytes:%d recvPackets:%d recvPacketsLost:%d recvFractionLost:%d recvCodecType:%d recvJitterMS:%d recvAudioLevel:%d",recvBytes,recvPackets,recvPacketsLost,recvFractionLost,recvCodecType,recvJitterMS,recvAudioLevel);
        }
    }
    
    {
        //Video Statistics
        int  sendBytes; int  sendPackets;
        int  sendPacketsLost; int  sendFractionLost;
        int  sendRttMS;
        int  sendCodecType;
        int  sendFrameWidth; int  sendFrameHeight;
        int  sendBitrateBPS;
        int  sendFramerate;
        int  recvBytes; int  recvPackets;
        int  recvPacketsLost; int  recvFractionLost;
        int  recvCodecType;
        int  recvFrameWidth;
        int  recvFrameHeight;
        int  recvBitrateBPS;
        int  recvFramerate;
        
        int errorCodec = [portSIPSDK getVideoStatistics:_activeSessionId
                                              sendBytes:&sendBytes
                                            sendPackets:&sendPackets
                                        sendPacketsLost:&sendPacketsLost
                                       sendFractionLost:&sendFractionLost
                                              sendRttMS:&sendRttMS
                                          sendCodecType:&sendCodecType
                                         sendFrameWidth:&sendFrameWidth
                                        sendFrameHeight:&sendFrameHeight
                                         sendBitrateBPS:&sendBitrateBPS
                                          sendFramerate:&sendFramerate
                                              recvBytes:&recvBytes
                                            recvPackets:&recvPackets
                                        recvPacketsLost:&recvPacketsLost
                                       recvFractionLost:&recvFractionLost
                                          recvCodecType:&recvCodecType
                                         recvFrameWidth:&recvFrameWidth
                                        recvFrameHeight:&recvFrameHeight
                                         recvBitrateBPS:&recvBitrateBPS
                                          recvFramerate:&recvFramerate];
        
        if(errorCodec == 0)
        {
            NSLog(@"Video Send Statistics sendBytes:%d sendPackets:%d sendPacketsLost:%d sendFractionLost:%d sendRttMS:%d sendCodecType:%d sendFrameWidth:%d sendFrameHeight:%d sendBitrateBPS:%d sendFramerate:%d ", sendBytes,sendPackets,sendPacketsLost,sendFractionLost,sendRttMS,sendCodecType,sendFrameWidth,sendFrameHeight,sendBitrateBPS,sendFramerate);
            NSLog(@"Video Received Statistics  recvBytes:%d recvPackets:%d recvPacketsLost:%d recvFractionLost:%d recvCodecType:%d recvFrameWidth:%d recvFrameHeight:%d recvBitrateBPS:%d recvFramerate:%d", recvBytes,recvPackets,recvPacketsLost,recvFractionLost,recvCodecType,recvFrameWidth,recvFrameHeight,recvBitrateBPS,recvFramerate);
        }
    }
}

- (void) updateCall
{
    [portSIPSDK updateCall:_activeSessionId enableAudio:YES enableVideo:YES];
}

- (void) makeTest
{
    [portSIPSDK audioPlayLoopbackTest:YES];
}

- (void) attendedRefer:(NSString*)referTo
{
    HSSession* session = [_callManager findAnotherCall:_activeSessionId];
    if(session)
    {
        [portSIPSDK attendedRefer:_activeSessionId replaceSessionId:session.sessionId referTo:referTo];
    }
    
}

- (void) muteCall:(BOOL)mute
{
    if(_activeSessionId != INVALID_SESSION_ID )
    {
        [_callManager muteCall:_activeSessionId muted:mute];
    }
    
    if (_isConference) {
        [_callManager muteAllCall:mute];
    }
}

- (void) setLoudspeakerStatus:(BOOL)enable
{
    [portSIPSDK setLoudspeakerStatus:enable];
}


- (BOOL)createConference:(PortSIPVideoRenderView*)conferenceVideoWindow
{
    if([_callManager createConference:conferenceVideoWindow videoWidth:352 videoHeight:288 displayLocalVideo:YES])
    {
        _isConference = YES;
        return YES;
    };
    return NO;
}

- (void)setConferenceVideoWindow:(PortSIPVideoRenderView*)conferenceVideoWindow
{
    [portSIPSDK setConferenceVideoWindow:conferenceVideoWindow];
}

- (void)destoryConference:(UIView *)viewRemoteVideo
{
    [_callManager destoryConference];
    HSSession* session = [_callManager findCallBySessionID:_activeSessionId];
    if(session && session.holdState)
    {
        [_callManager holdCall:session.sessionId onHold:NO];
    }
    _isConference = NO;
}

- (void)didSelectLine:(NSInteger)activeLine
{
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    
    [tabBarController dismissViewControllerAnimated:YES completion:nil];
    
    if (!sipRegistered || _activeLine == activeLine)
    {
        return;
    }
    
    if (!_isConference)
    {// Need to hold this line
        [_callManager holdCall:_activeSessionId onHold:YES];
    }
    
    _activeLine = activeLine;
    _activeSessionId = _lineSessions[_activeLine];
    
    //[loginViewController.buttonLine setTitle:[NSString  stringWithFormat:@"Line%zd:", activeLine] forState:UIControlStateNormal];
    
    if (!_isConference && _activeSessionId != INVALID_SESSION_ID)
    {
        // Need to unhold this line
        [_callManager holdCall:_activeSessionId onHold:NO];
    }
    
}


#pragma mark - CallManager delegate Implement
- (void)onIncomingCallWithoutCallKit:(long)sessionId
                         existsVideo:(BOOL)existsVideo
                         remoteParty:(NSString*)remoteParty
                   remoteDisplayName:(NSString*)remoteDisplayName
{//Call this by CallManager
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if(session == nil)
        return;
    
    if ([UIApplication sharedApplication].applicationState ==  UIApplicationStateBackground && !_enablePushNotification) {
        //Is Background and not enable CallKit, show the Notification
        UILocalNotification* localNotif = [[UILocalNotification alloc] init];
        
        if (localNotif){
            NSString *stringAlert = [NSString stringWithFormat:@"%@ \n<%@>%@", NSLocalizedString(@"Call from", @"Call from"), remoteDisplayName,remoteParty];
            if (existsVideo)
            {
                stringAlert = [NSString stringWithFormat:@"%@ \n<%@>%@", NSLocalizedString(@"Video call from", @"Video call from"), remoteDisplayName,remoteParty];
            }
            localNotif.soundName = @"ringtone29.mp3";
            
            localNotif.alertBody = stringAlert;
            localNotif.repeatInterval = 0;
            [[UIApplication sharedApplication]  presentLocalNotificationNow:localNotif];
        }
    }
    //Not support callkit,show the incoming Alert
    //Not support callkit,show the incoming Alert
    int index = [self findSession:sessionId];
    if(index  < 0)
    {//not found this session
        return ;
    }
    
    if(existsVideo)
    {
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Incoming Call"
                              message: [NSString  stringWithFormat:@"Call from <%@>%@ on line %d", remoteDisplayName,remoteParty,index]
                              delegate: self
                              cancelButtonTitle: @"Reject"
                              otherButtonTitles:@"Answer", @"Video",nil];
        alert.tag = index;
        [alert show];
        
//        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main"          bundle:nil];
//        IncomingViewController* incomViewController = [mainstoryboard      instantiateViewControllerWithIdentifier:@"IncomingViewController"];
//        [self.window.rootViewController presentViewController:incomViewController animated:NO completion:nil];
//

        
//        NSString* code = @"incoming";
//        NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
//        [settings setObject:code  forKey:@"code"];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Incoming Call"
                              message: [NSString  stringWithFormat:@"Call from <%@>%@ on line %d", remoteDisplayName,remoteParty,index]
                              delegate: self
                              cancelButtonTitle: @"Reject"
                              otherButtonTitles:@"Answer", nil];
        alert.tag = index;
        [alert show];
    }
    [_mSoundService playRingTone];
}

- (void)onNewOutgoingCall:(long)sessionId
{
    _lineSessions[_activeLine] = sessionId;
}

- (void)onAnsweredCall:(long)sessionId
{
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if(session)
    {
        if (session.videoCall) {
            
            //            [loginViewController onStartVideo:sessionId];
            [self setLoudspeakerStatus:YES];
        }
        else
        {
            [self setLoudspeakerStatus:NO];
        }
        
        int line = [self findSession:sessionId];
        if(line >= 0)
        {
            [self didSelectLine:line];
        }
    }
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    
    if (_activeSessionId == INVALID_SESSION_ID) {
        _activeSessionId = sessionId ;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Call Established on line  %d",[self findSession:sessionId]]];
}

-(void)onCloseCall:(long)sessionId
{
    
    
    [self freeLine:sessionId];
    
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if(session)
    {
        [_callManager removeCall:session];
        
        if (session.videoCall) {
            //            [loginViewController onStopVideo:sessionId];
            //            [loginViewController layerVideo];
            
            //        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main"          bundle:nil];
            //        EndCallViewController* EndCall = [mainstoryboard      instantiateViewControllerWithIdentifier:@"EndCallViewController"];
            //        [self.window.rootViewController presentViewController:EndCall animated:YES completion:nil];
            
        }
        
        
    }
    
    if(sessionId == _activeSessionId)
    {
        _activeSessionId = INVALID_SESSION_ID;
    }
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    
    
    if([_callManager getConnectCallNum] == 0)
    {
        //Setting speakers for sound output (The system default behavior)
        [self setLoudspeakerStatus:YES];
    }
    
    
    
    
}

- (void)onMuteCall:(long)sessionId muted:(BOOL)muted
{
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if(session)
    {//update Mute status
        
    }
}

- (void)onHoldCall:(long)sessionId onHold:(BOOL)onHold
{
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if(session && sessionId == _activeSessionId)
    {//update Hold status
        if(onHold)
        {
            [portSIPSDK setRemoteVideoWindow:sessionId remoteVideoWindow:nil];
            
            //[loginViewController setStatusText:[NSString  stringWithFormat:@"Hold call on line %zd", _activeLine]];
        }
        else
        {
            //            [portSIPSDK setRemoteVideoWindow:sessionId remoteVideoWindow:loginViewController.viewRemoteVideo];
            
            
            //[loginViewController setStatusText:[NSString  stringWithFormat:@"unHold call on line %zd",_activeLine]];
        }
    }
}

#pragma mark - PortSIPSDK sip callback events Delegate
//Register Event
- (void)onRegisterSuccess:(char*) statusText statusCode:(int)statusCode sipMessage:(char *)sipMessage

{
    sipRegistered = YES;
    //    [loginViewController onRegisterSuccess:statusCode withStatusText:statusText];
    
};

- (void)onRegisterFailure:(char*) statusText statusCode:(int)statusCode sipMessage:(char *)sipMessage
{
    sipRegistered = NO;
    //    [loginViewController onRegisterFailure:statusCode withStatusText:statusText];
    [self endBackgroundTaskForRegister];
    
};


//Call Event
- (void)onInviteIncoming:(long)sessionId
       callerDisplayName:(char*)callerDisplayName
                  caller:(char*)caller
       calleeDisplayName:(char*)calleeDisplayName
                  callee:(char*)callee
             audioCodecs:(char*)audioCodecs
             videoCodecs:(char*)videoCodecs
             existsAudio:(BOOL)existsAudio
             existsVideo:(BOOL)existsVideo
              sipMessage:(char *)sipMessage
{
    int num = [_callManager getConnectCallNum];
    int index = [self findIdleLine];
    if (num >= MAX_LINES || index < 0) {
        [portSIPSDK rejectCall:sessionId code:486];
        return;
    }
    
    NSString* remoteParty = [[NSString alloc] initWithCString:(const char*)caller encoding:NSASCIIStringEncoding];
    NSString* remoteDisplayName = [[NSString alloc] initWithCString:(const char*)callerDisplayName encoding:NSASCIIStringEncoding];
    
    
    HSSession *session = [[HSSession alloc] initWithSessionIdAndUUID:sessionId callUUID:nil remoteParty:remoteParty displayName:remoteDisplayName videoState:existsVideo callOut:NO];
    
    
    _lineSessions[index] = sessionId;
    [_callManager addCall:session];
    
    
    [_callManager incomingCall:sessionId existsVideo:existsVideo remoteParty:remoteParty remoteDisplayName:remoteDisplayName];
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Incoming call:%@ on line %d",remoteParty, index]];
};

- (void)onInviteTrying:(long)sessionId
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Call is trying on line %d",index]];
};

- (void)onInviteSessionProgress:(long)sessionId
                    audioCodecs:(char*)audioCodecs
                    videoCodecs:(char*)videoCodecs
               existsEarlyMedia:(BOOL)existsEarlyMedia
                    existsAudio:(BOOL)existsAudio
                    existsVideo:(BOOL)existsVideo
                     sipMessage:(char *)sipMessage
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    if (existsEarlyMedia)
    {
        // Checking does this call has video
        if (existsVideo)
        {
            // This incoming call has video
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them by yourself.
        }
        
        if (existsAudio)
        {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them by yourself.
        }
    }
    
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    
    session.existEarlyMedia = existsEarlyMedia;
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Call session progress on line %d",index]];
}

- (void)onInviteRinging:(long)sessionId
             statusText:(char*)statusText
             statusCode:(int)statusCode
             sipMessage:(char *)sipMessage
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if (!session.existEarlyMedia)
    {
        // No early media, you must play the local WAVE file for ringing tone
        [_mSoundService playRingBackTone];
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Call ringing on line %d",index]];
}

- (void)onInviteAnswered:(long)sessionId
       callerDisplayName:(char*)callerDisplayName
                  caller:(char*)caller
       calleeDisplayName:(char*)calleeDisplayName
                  callee:(char*)callee
             audioCodecs:(char*)audioCodecs
             videoCodecs:(char*)videoCodecs
             existsAudio:(BOOL)existsAudio
             existsVideo:(BOOL)existsVideo
              sipMessage:(char*)sipMessage
{
    // If more than one codecs using, then they are separated with "#",
    // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them by yourself.
    // Checking does this call has video
    if (existsVideo)
    {
        //        [loginViewController onStartVideo:sessionId];
    }
    
    if (existsAudio)
    {
    }
    
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    [_callManager answerCall:sessionId isVideo:existsVideo];
}

- (void)onInviteFailure:(long)sessionId
                 reason:(char*)reason
                   code:(int)code
             sipMessage:(char *)sipMessage
{
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    
    //    [loginViewController layerVideo];
    
    if (session == nil) {
        NSLog(@"Not exist this SessionId = %ld", sessionId);
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Failed to call on line  %d,%s(%d)",[self findSession:sessionId],reason,code]];
    
    if (session.isReferCall)
    {
        // Take off the origin call from HOLD if the refer call is failure
        HSSession* originSession = [_callManager findCallByOrignalSessionID:session.orignalId];
        
        if (originSession != nil)
        {
            // [loginViewController setStatusText:[NSString  stringWithFormat:@"Call failure on line  %d,%s(%d)",[self findSession:sessionId],reason,code]];
            
            // Now take off the origin call
            [portSIPSDK unHold:originSession.sessionId];
            
            originSession.holdState = NO;
            
            // Switch the currently line to origin call line
            _activeLine = [self findSession:sessionId];
            
            NSLog(@"Current line is: %zd",_activeLine);
        }
    }
    
    [_callManager endCall:sessionId];
}

- (void)onInviteUpdated:(long)sessionId
            audioCodecs:(char*)audioCodecs
            videoCodecs:(char*)videoCodecs
            existsAudio:(BOOL)existsAudio
            existsVideo:(BOOL)existsVideo
             sipMessage:(char*)sipMessage
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    // Checking does this call has video
    if (existsVideo)
    {
        //        [loginViewController onStartVideo:sessionId];
    }
    if (existsAudio)
    {
    }
    
    
    ///[loginViewController setStatusText:[NSString  stringWithFormat:@"The call has been updated on line %d",[self findSession:sessionId]]];
}

- (void)onInviteConnected:(long)sessionId
{
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    
    if (session == nil) {
        NSLog(@"Not exist this SessionId = %ld", sessionId);
        return;
    }
    
    // [loginViewController setStatusText:[NSString  stringWithFormat:@"The call is connected on line %d",[self findSession:sessionId]]];
    if (session.videoCall) {
        [self setLoudspeakerStatus:YES];
    }
    else{
        [self setLoudspeakerStatus:NO];
        //        [loginViewController RubahStatus];
    }
    NSLog(@"onInviteConnected...");
}


- (void)onInviteBeginingForward:(char*)forwardTo
{
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Call has been forward to:%s" ,forwardTo]];
}

- (void)onInviteClosed:(long)sessionId
{
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Call closed by remote on line %d",[self findSession:sessionId]]];
    NSString *codelocalize;
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    codelocalize = [settings stringForKey:@"code"];
    
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    if (session != nil) {
        [_callManager endCall:sessionId];
        
    }
    
    if ([codelocalize  isEqual: @"incoming"])
    {
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
        //    [loginViewController unRegister];
    }
    else
    {
        
        //        [loginViewController unRegister];
        
    }
    
    NSLog(@"onInviteClosed...");
    
    
    
}

- (void)onDialogStateUpdated:(char*)BLFMonitoredUri
              BLFDialogState:(char*)BLFDialogState
                 BLFDialogId:(char*) BLFDialogId
          BLFDialogDirection:(char*) BLFDialogDirection
{
    NSLog(@"The user %s dialog state is updated:%s, dialog id: %s, direction: %s ",
          BLFMonitoredUri,BLFDialogState,BLFDialogId,BLFDialogDirection);
}

- (void)onRemoteHold:(long)sessionId
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Placed on hold by remote on line %d",index]];
}

- (void)onRemoteUnHold:(long)sessionId
           audioCodecs:(char*)audioCodecs
           videoCodecs:(char*)videoCodecs
           existsAudio:(BOOL)existsAudio
           existsVideo:(BOOL)existsVideo
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Take off hold by remote on line  %d",index]];
}

//Transfer Event
- (void)onReceivedRefer:(long)sessionId
                referId:(long)referId
                     to:(char*)to
                   from:(char*)from
        referSipMessage:(char*)referSipMessage
{
    HSSession* session = [_callManager findCallBySessionID:sessionId];
    
    if (session == nil) {
        //Not found the refer session, reject refer.
        [portSIPSDK rejectRefer:referId];
        NSLog(@"Not exist this SessionId = %ld", sessionId);
        return;
    }
    
    int index = [self findIdleLine];
    if (index < 0) {
        //Not found the idle line, reject refer.
        [portSIPSDK rejectRefer:referId];
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Received the refer on line %d, refer to %s",[self findSession:sessionId],to]];
    
    //auto accept refer
    long referSessionId = [portSIPSDK acceptRefer:referId referSignaling:[NSString stringWithUTF8String:referSipMessage]];
    if (referSessionId <= 0)
    {
        //[loginViewController setStatusText:[NSString  stringWithFormat:@"Failed to accept the refer."]];
    }
    else
    {
        [_callManager endCall:sessionId];
        
        NSString* remote = [NSString stringWithUTF8String:to];
        HSSession *session = [[HSSession alloc] initWithSessionIdAndUUID:referSessionId callUUID:nil remoteParty:remote displayName:remote videoState:YES callOut:YES];
        
        [_callManager addCall:session];
        _lineSessions[index] = referSessionId;
        
        session.sessionState = YES;
        session.isReferCall = YES;
        session.orignalId = sessionId;
        
        //[loginViewController setStatusText:[NSString  stringWithFormat:@"Accepted the refer, new call is trying on line %d",index]];
    }
    
    
    /*if you want to reject Refer
     [mPortSIPSDK rejectRefer:referId];
     [loginViewController setStatusText:@"Rejected the the refer."];
     */
}

- (void)onReferAccepted:(long)sessionId
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Line %d, the REFER was accepted.",index]];
}

- (void)onReferRejected:(long)sessionId reason:(char*)reason code:(int)code
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Line %d, the REFER was rejected.",index]];
}

- (void)onTransferTrying:(long)sessionId
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Transfer trying on line %d",index]];
}

- (void)onTransferRinging:(long)sessionId
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Transfer ringing on line %d",index]];
}

- (void)onACTVTransferSuccess:(long)sessionId
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Transfer succeeded on line %d",index]];
    
    //Transfer has success, hangup call.
    [portSIPSDK hangUp:sessionId];
}

- (void)onACTVTransferFailure:(long)sessionId reason:(char*)reason code:(int)code
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Failed to transfer on line %d",index]];
}

//Signaling Event
- (void)onReceivedSignaling:(long)sessionId message:(char*)message
{
    // This event will be fired when the SDK received a SIP message
    // you can use signaling to access the SIP message.
}

- (void)onSendingSignaling:(long)sessionId message:(char*)message
{
    // This event will be fired when the SDK sent a SIP message
    // you can use signaling to access the SIP message.
}

- (void)onWaitingVoiceMessage:(char*)messageAccount
        urgentNewMessageCount:(int)urgentNewMessageCount
        urgentOldMessageCount:(int)urgentOldMessageCount
              newMessageCount:(int)newMessageCount
              oldMessageCount:(int)oldMessageCount
{
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Has voice messages,%s(%d,%d,%d,%d)",messageAccount,urgentNewMessageCount,urgentOldMessageCount,newMessageCount,oldMessageCount]];
}

- (void)onWaitingFaxMessage:(char*)messageAccount
      urgentNewMessageCount:(int)urgentNewMessageCount
      urgentOldMessageCount:(int)urgentOldMessageCount
            newMessageCount:(int)newMessageCount
            oldMessageCount:(int)oldMessageCount
{
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Has Fax messages,%s(%d,%d,%d,%d)",messageAccount,urgentNewMessageCount,urgentOldMessageCount,newMessageCount,oldMessageCount]];
}

- (void)onRecvDtmfTone:(long)sessionId tone:(int)tone
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Received DTMF tone: %d  on line %d",tone, index]];
}

- (void)onRecvOptions:(char*)optionsMessage
{
    NSLog(@"Received an OPTIONS message:%s",optionsMessage);
}

- (void)onRecvInfo:(char*)infoMessage
{
    NSLog(@"Received an INFO message:%s",infoMessage);
}

- (void)onRecvNotifyOfSubscription:(long)subscribeId
                     notifyMessage:(char*)notifyMessage
                       messageData:(unsigned char*)messageData
                 messageDataLength:(int)messageDataLength
{
    NSLog(@"Received an Notify message");
}
//Instant Message/Presence Event
//Instant Message/Presence Event
- (void)onPresenceRecvSubscribe:(long)subscribeId
                fromDisplayName:(char*)fromDisplayName
                           from:(char*)from
                        subject:(char*)subject
{
    //[imViewController onPresenceRecvSubscribe:subscribeId fromDisplayName:fromDisplayName from:from subject:subject];
}

- (void)onPresenceOnline:(char*)fromDisplayName
                    from:(char*)from
               stateText:(char*)stateText
{
    //[imViewController onPresenceOnline:fromDisplayName from:from
    // stateText:stateText];
}


- (void)onPresenceOffline:(char*)fromDisplayName from:(char*)from
{
    //[imViewController onPresenceOffline:fromDisplayName from:from];
}


- (void)onRecvMessage:(long)sessionId
             mimeType:(char*)mimeType
          subMimeType:(char*)subMimeType
          messageData:(unsigned char*)messageData
    messageDataLength:(int)messageDataLength
{
    int index = [self findSession:sessionId];
    if (index == -1)
    {
        return;
    }
    
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Received a MESSAGE message on line %d",index]];
    
    
    if (strcmp(mimeType,"text") == 0 && strcmp(subMimeType,"plain") == 0)
    {
        NSString* recvMessage = [NSString stringWithUTF8String:(const char*)messageData];
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"recvMessage"
                              message: recvMessage
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (strcmp(mimeType,"application") == 0 && strcmp(subMimeType,"vnd.3gpp.sms") == 0)
    {
        // The messageData is binary data
    }
    else if (strcmp(mimeType,"application") == 0 && strcmp(subMimeType,"vnd.3gpp2.sms") == 0)
    {
        // The messageData is binary data
    }
}

- (void)onRecvOutOfDialogMessage:(char*)fromDisplayName
                            from:(char*)from
                   toDisplayName:(char*)toDisplayName
                              to:(char*)to
                        mimeType:(char*)mimeType
                     subMimeType:(char*)subMimeType
                     messageData:(unsigned char*)messageData
               messageDataLength:(int)messageDataLength
                      sipMessage:(char*)sipMessage
{
    //[loginViewController setStatusText:[NSString  stringWithFormat:@"Received a message(out of dialog) from %s",from]];
    
    if (strcasecmp(mimeType,"text") == 0 && strcasecmp(subMimeType,"plain") == 0)
    {
        NSString* recvMessage = [NSString stringWithUTF8String:(const char*)messageData];
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:[NSString  stringWithUTF8String:from]
                              message: recvMessage
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (strcasecmp(mimeType,"application") == 0 && strcasecmp(subMimeType,"vnd.3gpp.sms") == 0)
    {
        // The messageData is binary data
    }
    else if (strcasecmp(mimeType,"application") == 0 && strcasecmp(subMimeType,"vnd.3gpp2.sms") == 0)
    {
        // The messageData is binary data
    }
}

- (void)onSendMessageSuccess:(long)sessionId messageId:(long)messageId
{
    //[imViewController onSendMessageSuccess:messageId];
}


- (void)onSendMessageFailure:(long)sessionId messageId:(long)messageId reason:(char*)reason code:(int)code
{
    //[imViewController onSendMessageFailure:messageId reason:reason code:code];
}

- (void)onSendOutOfDialogMessageSuccess:(long)messageId
                        fromDisplayName:(char*)fromDisplayName
                                   from:(char*)from
                          toDisplayName:(char*)toDisplayName
                                     to:(char*)to
{
    //[imViewController onSendMessageSuccess:messageId];
}


- (void)onSendOutOfDialogMessageFailure:(long)messageId
                        fromDisplayName:(char*)fromDisplayName
                                   from:(char*)from
                          toDisplayName:(char*)toDisplayName
                                     to:(char*)to
                                 reason:(char*)reason
                                   code:(int)code
{
    //[imViewController onSendMessageFailure:messageId reason:reason code:code];
}

- (void)onSubscriptionFailure:(long)subscribeId
                   statusCode:(int)statusCode
{
    NSLog(@"SubscriptionFailure subscribeId=%ld statusCode:%d",subscribeId, statusCode);
}
//Play file event
- (void)onSubscriptionTerminated:(long)subscribeId
{
    NSLog(@"onSubscriptionTerminated subscribeId=%ld",subscribeId);
}

//Play file event
- (void)onPlayAudioFileFinished:(long)sessionId fileName:(char*)fileName
{
    
}

- (void)onPlayVideoFileFinished:(long)sessionId
{
    
}

//RTP/Audio/video stream callback data
- (void)onReceivedRTPPacket:(long)sessionId isAudio:(BOOL)isAudio RTPPacket:(unsigned char *)RTPPacket packetSize:(int)packetSize
{
    /* !!! IMPORTANT !!!
     
     Don't call any PortSIP SDK API functions in here directly. If you want to call the PortSIP API functions or
     other code which will spend long time, you should post a message to main thread(main window) or other thread,
     let the thread to call SDK API functions or other code.
     */
}

- (void)onSendingRTPPacket:(long)sessionId isAudio:(BOOL)isAudio RTPPacket:(unsigned char *)RTPPacket packetSize:(int)packetSize
{
    /* !!! IMPORTANT !!!
     
     Don't call any PortSIP SDK API functions in here directly. If you want to call the PortSIP API functions or
     other code which will spend long time, you should post a message to main thread(main window) or other thread,
     let the thread to call SDK API functions or other code.
     */
}

- (void)onAudioRawCallback:(long)sessionId
         audioCallbackMode:(int)audioCallbackMode
                      data:(unsigned char *)data
                dataLength:(int)dataLength
            samplingFreqHz:(int)samplingFreqHz
{
    /* !!! IMPORTANT !!!
     
     Don't call any PortSIP SDK API functions in here directly. If you want to call the PortSIP API functions or
     other code which will spend long time, you should post a message to main thread(main window) or other thread,
     let the thread to call SDK API functions or other code.
     */
}

- (int)onVideoRawCallback:(long)sessionId
        videoCallbackMode:(int)videoCallbackMode
                    width:(int)width
                   height:(int)height
                     data:(unsigned char *)data
               dataLength:(int)dataLength
{
    /* !!! IMPORTANT !!!
     
     Don't call any PortSIP SDK API functions in here directly. If you want to call the PortSIP API functions or
     other code which will spend long time, you should post a message to main thread(main window) or other thread,
     let the thread to call SDK API functions or other code.
     */
    return 0;
}

- (void)alertView: (UIAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    NSInteger index = 0;
    HSSession* session = [_callManager findCallBySessionID:_lineSessions[index]];
    if(buttonIndex == 0){//reject Call
        [_callManager endCall:session.sessionId ];
        
        return;
    }
    
    
    BOOL videoCall = buttonIndex == 2;
    if([_callManager answerCall:session.sessionId isVideo:videoCall])
    {
        
    }
    else{
        
    };
}


- (void)AnswareVideo
{
    
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    NSInteger index = 0;
    HSSession* session = [_callManager findCallBySessionID:_lineSessions[index]];
    [_callManager answerCall:session.sessionId isVideo:YES];
    
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (void)Reject
{
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    NSInteger index = 0;
    HSSession* session = [_callManager findCallBySessionID:_lineSessions[index]];
    [_callManager endCall:session.sessionId ];
    
    [self EndView];
    //    [loginViewController unRegister];
    
    
}

- (void)RejectPermission
{
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    NSInteger index = 0;
    HSSession* session = [_callManager findCallBySessionID:_lineSessions[index]];
    [_callManager endCall:session.sessionId ];
    
    
    //    [loginViewController unRegister];
    
    
}
-(void)StopRing
{
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
}
- (void)RejectAuto
{
    [_mSoundService stopRingTone];
    [_mSoundService stopRingBackTone];
    
    NSInteger index = 0;
    HSSession* session = [_callManager findCallBySessionID:_lineSessions[index]];
    [_callManager endCall:session.sessionId ];
    //    [loginViewController unRegister];
    
    
    
}


#pragma mark - Network Status
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    
    switch (netStatus) {
        case NotReachable:
            NSLog(@"reachabilityChanged:kNotReachable");
            //            [loginViewController ErrorConnection];
            break;
        case ReachableViaWWAN:
            //            [loginViewController refreshRegister];
            NSLog(@"reachabilityChanged:kReachableViaWWAN");
            //            [loginViewController Connection];
            break;
        case ReachableViaWiFi:
            //            [loginViewController refreshRegister];
            NSLog(@"reachabilityChanged:kReachableViaWiFi");
            //            [loginViewController Connection];
            break;
        default:
            break;
    }
}

- (void)startNotifierNetwork
{
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    [internetReach startNotifier];
}

- (void)stopNotifierNetwork
{
    [internetReach stopNotifier];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - VoIP PUSH
-(void)addPushSupportWithPortPBX:(BOOL)enablePush
{
    if(_VoIPPushToken == nil&& !_enablePushNotification)
        return;
    
    //This VoIP Push is only work with PortPBX(https://www.portsip.com/portsip-pbx/)
    //if you want work with other PBX, please contact your PBX Provider
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    [portSIPSDK clearAddedSipMessageHeaders];
    NSString* pushMessage ;
    if(enablePush)
    {
        pushMessage = [[NSString alloc] initWithFormat:@"device-os=ios;device-uid=%@;allow-call-push=true;allow-message-push=true;app-id=%@",_VoIPPushToken,bundleIdentifier];
        
        NSLog(@"Enable pushMessage:{%@}",pushMessage);
    }
    else{
        pushMessage = [[NSString alloc] initWithFormat:@"device-os=ios;device-uid=%@;allow-call-push=false;allow-message-push=false;app-id=%@",_VoIPPushToken,bundleIdentifier];
        NSLog(@"Disable pushMessage:{%@}",pushMessage);
    }
    
    [portSIPSDK addSipMessageHeader:-1 methodName:@"REGISTER" msgType:1 headerName:@"portsip-push" headerValue:pushMessage];
}


- (void) refreshPushStatusToSipServer:(BOOL)addPushHeader
{
    if(addPushHeader){
        [self addPushSupportWithPortPBX:YES];
    }
    else{
        //remove push header
        [portSIPSDK clearAddedSipMessageHeaders];
    }
    
    
    [self refreshRegister];
}


-(void)processPushMessageFromPortPBX:(NSDictionary *)dictionaryPayload
{
    /* dictionaryPayload JSON Format
     Payload: {
     "message_id" = "96854b5d-9d0b-4644-af6d-8d97798d9c5b";
     "msg_content" = "Received a call.";
     "msg_title" = "Received a new call";
     "msg_type" = "call";// im message is "im"
     "portsip-push-id" = "pvqxCpo-j485AYo9J1cP5A..";
     "send_from" = "102";
     "send_to" = "sip:105@portsip.com";
     }
     */
    
    NSDictionary *parsedObject = dictionaryPayload;
    
    bool isCall = YES;
    NSString *msgType = [parsedObject valueForKey:@"msg_type"];
    //if(msgType.count > 0 && [msgType[0]  isEqual: @"call"])
    if(msgType.length > 0 && [msgType isEqual: @"im"])
    {
        isCall = NO;
    }
    
    if(isCall)
    {
        if(!_callManager.enableCallKit)
        {//If not enable Call Kit, show the local Notification
            UILocalNotification *backgroudMsg = [[ UILocalNotification alloc] init];
            
            NSString *sendFrom = [parsedObject valueForKey:@"send_from"];
            NSString *sendTo = [parsedObject valueForKey:@"send_to"];
            
            NSString* alertBody = [NSString stringWithFormat:@"You receive a new call From:%@ To:%@", sendFrom, sendTo];
            
            backgroudMsg.alertBody= alertBody;
            backgroudMsg.soundName = @"ringtone.mp3" ;
            backgroudMsg.applicationIconBadgeNumber = [[ UIApplication  sharedApplication]applicationIconBadgeNumber] + 1 ;
            [[ UIApplication sharedApplication] presentLocalNotificationNow:backgroudMsg];
        }
        else{
            //            [loginViewController refreshRegister];
            [self beginBackgroundTaskForRegister];
        }
    }
    else{
        //received a SIP Page Message, show it
        UILocalNotification *backgroudMsg = [[ UILocalNotification alloc] init];
        NSString *sendFrom = [parsedObject valueForKey:@"send_from"];
        NSString *sendTo = [parsedObject valueForKey:@"send_to"];
        NSString *content = [parsedObject valueForKey:@"msg_content"];
        NSString *title = [parsedObject valueForKey:@"msg_title"];
        
        NSString* alertBody= [NSString stringWithFormat:@"You receive a new message From:%@ To:%@ %@", sendFrom, sendTo, content];
        
        backgroudMsg.alertTitle =title;
        backgroudMsg.alertBody= alertBody;
        backgroudMsg.applicationIconBadgeNumber = [[ UIApplication  sharedApplication]applicationIconBadgeNumber] + 1 ;
        [[ UIApplication sharedApplication] presentLocalNotificationNow:backgroudMsg];
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    PKPushRegistry *pushRegistry = [[PKPushRegistry alloc] initWithQueue:nil];
    pushRegistry.delegate = self;
    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(PKPushType)type
{
    NSString *token = [NSString stringWithFormat:@"%@", credentials.token];
    
    _VoIPPushToken = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    _VoIPPushToken = [_VoIPPushToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"didUpdatePushCredentials:%@", token);
    [self refreshPushStatusToSipServer:YES];
}

- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type
{
    if (sipRegistered &&
        ([UIApplication sharedApplication].applicationState ==  UIApplicationStateActive ||
         [_callManager getConnectCallNum] > 0) )
    {
        NSLog(@"didReceiveIncomingPushWith:ignore push message when ApplicationStateActive or have active call. Payload: %@", payload.dictionaryPayload);
        return ;
    }
    
    NSLog(@"Payload: %@", payload.dictionaryPayload);
    
    
    [self processPushMessageFromPortPBX:payload.dictionaryPayload];
};



- (void) beginBackgroundTaskForRegister
{
    _backtaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundTaskForRegister];
    }];
    int interval = 5;//waiting 5 sec, stop endBackgroundTaskForRegister
    [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(endBackgroundTaskForRegister) userInfo:nil repeats:NO];
    NSLog(@"beginBackgroundTaskForRegister");
}

- (void) endBackgroundTaskForRegister
{
    if(_backtaskIdentifier != UIBackgroundTaskInvalid)
    {
        [[UIApplication sharedApplication] endBackgroundTask: _backtaskIdentifier];
        _backtaskIdentifier = UIBackgroundTaskInvalid;
        NSLog(@"endBackgroundTaskForRegister");
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    
    NSLog(@"applicationDidEnterBackground");
    if(_enableForceBackground)
    {//Disable to save battery, or when you don't need incoming calls while APP is in background.
        [portSIPSDK startKeepAwake];
        [self holdCall];
        //[_callManager holdAllCall:YES];
        NSLog(@"Pause Video");
    }
    else
    {
        //        [loginViewController unRegister];
        [self beginBackgroundTaskForRegister];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"applicationWillEnterForeground");
    if(_enableForceBackground)
    {
        [portSIPSDK stopKeepAwake];
        [self unholdCall];
        //[_callManager holdAllCall:NO];
        NSLog(@"Start Video");
    }
    else
    {
        //        [loginViewController refreshRegister];
        
    }
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if(_enablePushNotification)
    {
        [portSIPSDK unRegisterServer];
        [NSThread sleepForTimeInterval:3.0];
        NSLog(@"applicationWillTerminate");
    }
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    return YES;
}

// Called on the main thread after the NSUserActivity object is available. Use the data you stored in the NSUserActivity object to re-create what the user was doing.
// You can create/fetch any restorable objects associated with the user activity, and pass them to the restorationHandler. They will then have the UIResponder restoreUserActivityState: method
// invoked with the user activity. Invoking the restorationHandler is optional. It may be copied and invoked later, and it will bounce to the main thread to complete its work and call
// restoreUserActivityState on all objects.
- (BOOL)application:(UIApplication *)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler
{
    if(![userActivity.activityType isEqualToString:@"INStartVideoCallIntent"] &&
       ![userActivity.activityType isEqualToString:@"INStartAudioCallIntent"])
    {
        return NO;
    }
    
    BOOL isVideo = NO;
    if([userActivity.activityType isEqualToString:@"INStartVideoCallIntent"])
    {
        isVideo = YES;
    }
    
    INInteraction *interaction = userActivity.interaction;
    INStartAudioCallIntent *startAudioCallIntent = (INStartAudioCallIntent *)interaction.intent;
    //INStartVideoCallIntent *startVideoCallIntent = (INStartVideoCallIntent *)interaction.intent;
    INPerson *contact = startAudioCallIntent.contacts[0];
    INPersonHandle *personHandle = contact.personHandle;
    NSString *phoneNumber = personHandle.value;
    
    [self makeCall:phoneNumber videoCall:isVideo];
    
    return YES;
}


- (void)add:(CDVInvokedUrlCommand*)command
{
    // Override point for customization after application launch.
    NSDictionary *defaultValues = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @NO, @"CallKit",
                                   @YES, @"PushNotification",
                                   @YES, @"ForceBackground",nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
    
    //load the settings
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    BOOL enableCallKit = [settings boolForKey:@"CallKit"];
    _enablePushNotification = [settings boolForKey:@"PushNotification"];
    _enableForceBackground = [settings boolForKey:@"ForceBackground"];
    
    // Override point for customization after application launch.
    _mSoundService = [[SoundService alloc] init];
    
    portSIPSDK = [[PortSIPSDK alloc] init];
    portSIPSDK.delegate = self;
    
    
    _cxProvide = [PortCxProvider sharedInstance];
    _callManager = [[CallManager alloc] initWithSDK:portSIPSDK];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0){
        _callManager.enableCallKit = enableCallKit;
        
        
    }
    
    _callManager.delegate = self;
    _cxProvide.callManager = _callManager;
    
    _activeLine = 0;
    _activeSessionId = INVALID_SESSION_ID;
    for(int i = 0; i < MAX_LINES; i++)
    {
        _lineSessions[i] = INVALID_SESSION_ID;
    }
    
    sipRegistered = NO;
    
    _isConference = NO;
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [self startNotifierNetwork];
    
    
     [self Register];


    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Register Success"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    
    
}

- (void) Register
{
    portSIPSDK = [[PortSIPSDK alloc] init];
    portSIPSDK.delegate = self;
    
    
    UserName = @"1006";
    DisplayName = @"1006";
    AuthName = @"1006";
    Password = @"mk1234";
    UserDomain = @"30.0.0.13";
    SIPServer = @"30.0.0.13";
    SIPPort = @"5061";
    
    NSString* kUserName = UserName;
    NSString* kDisplayName = DisplayName;
    NSString* kAuthName = AuthName;
    NSString* kPassword = Password;
    NSString* kUserDomain = UserDomain;
    NSString* kSIPServer = SIPServer;
    NSString* Port = SIPPort;
    int kSIPServerPort = [Port intValue];
    
    
    TRANSPORT_TYPE transport = TRANSPORT_TLS;//TRANSPORT_TCP
    SRTP_POLICY srtp = SRTP_POLICY_FORCE;
    
    int localPort = 10002;
    NSString* loaclIPaddress = @"0.0.0.0";//Auto select IP address
    //NSString* loaclIPaddress = @"::";//Auto select IP address
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    int ret = [portSIPSDK initialize:transport localIP:loaclIPaddress localSIPPort:localPort loglevel:PORTSIP_LOG_NONE  logPath:documentsDirectory maxLine:8 agent:@"PortSIP SDK for IOS"  audioDeviceLayer:0 videoDeviceLayer:0 TLSCertificatesRootPath:@"" TLSCipherList:@"" verifyTLSCertificate:NO];
    
    if(ret != 0)
    {
        NSLog(@"initialize failure ErrorCode = %d",ret);
        return ;
    }
    
    ret = [portSIPSDK setUser:kUserName displayName:kDisplayName authName:kAuthName password:kPassword userDomain:kUserDomain SIPServer:kSIPServer SIPServerPort:kSIPServerPort STUNServer:@"" STUNServerPort:0 outboundServer:@"" outboundServerPort:0];
    
    if(ret != 0){
        NSLog(@"setUser failure ErrorCode = %d",ret);
        return ;
    }
    
    int rt = [portSIPSDK setLicenseKey:@"2iOSzB00MjY2MURCRUQzQUFFNUQyMTU1RDJCQjkzOTlBQTA4QUA1NDkxQTQ1MEQ1QUI5OTYxNzNFRDU5NUVFMkUxMDlEQUBERTg2QkI0NUZDNDEzMEE5QTRCRkY1MTc5MDE3NjU3RUAyQjBFM0NFNjJGNDg3M0RFNDcxNTg2QzFBNDY5M0JGNQ"];
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    if (state == UIApplicationStateBackground) {
        NSLog(@"setLicenseKey %d", rt);
    }else{
        if (rt == ECoreTrialVersionLicenseKey)
        {
            
            // UIAlertView *alert = [[UIAlertView alloc]
            // initWithTitle:@"Warning"
            //message: @"This trial version SDK just allows short conversation, you can't heairng anyting after 2-3 minutes, contact us: sales@portsip.com to buy official version."
            //delegate: self
            //cancelButtonTitle: @"OK"
            //otherButtonTitles:nil];
            //[alert show];
        }
        else if (rt == ECoreWrongLicenseKey)
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message: @"The wrong license key was detected"
                                  delegate: self
                                  cancelButtonTitle: @"OK"
                                  otherButtonTitles:nil];
            [alert show];
            NSLog(@"setLicenseKey failure ErrorCode = %d",rt);
            return ;
        }
        else if (rt == ECoreTrialVersionExpired)
        {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message: @"This trial version SDK has expired"
                                  delegate: self
                                  cancelButtonTitle: @"OK"
                                  otherButtonTitles:nil];
            [alert show];
            NSLog(@"setLicenseKey failure ErrorCode = %d",rt);
            return ;
        }
    }
    
    
    [portSIPSDK addAudioCodec:AUDIOCODEC_OPUS];
    [portSIPSDK addVideoCodec:VIDEO_CODEC_H264];
    
    [portSIPSDK setVideoBitrate:-1 bitrateKbps:500];//Default video send bitrate,500kbps
    [portSIPSDK setVideoFrameRate:-1 frameRate:10];//Default video frame rate,10
    [portSIPSDK setVideoResolution:352 height:288];
    [portSIPSDK setAudioSamples:20 maxPtime:60];//ptime 20
    
    // [portSIPSDK setInstanceId:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    
    if(_enablePushNotification)
    {
        [self addPushSupportWithPortPBX:YES];
    }
    
    //1 - FrontCamra 0 - BackCamra
    [portSIPSDK setVideoDeviceId:1];
    
    //enable video RTCP nack
    [portSIPSDK setVideoNackStatus:YES];
    
    //enable srtp
    [portSIPSDK setSrtpPolicy:srtp];
    
    // Try to register the default identity.
    // Registration refreshment interval is 90 seconds
    ret = [portSIPSDK registerServer:90 retryTimes:0];
    if(ret != 0){
        [portSIPSDK unInitialize];
        NSLog(@"registerServer failure ErrorCode = %d",ret);
        return ;
    }
    
    if(transport == TRANSPORT_TCP ||
       transport == TRANSPORT_TLS){
        [portSIPSDK setKeepAliveTime:0];
    }
    
    //[_activityIndicator startAnimating];
    
    //[_labelDebugInfo setText:@"Registration..."];
    NSString* sipURL = nil;
    if(kSIPServerPort == 5060)
        sipURL = [[NSString alloc] initWithFormat:@"sip:%@:%@",kUserName,kUserDomain];
    else
        sipURL = [[NSString alloc] initWithFormat:@"sip:%@:%@:%d",kUserName,kUserDomain,kSIPServerPort];
  
    self.sipURL = sipURL;
    
    sipInitialized = YES;
    sipRegistrationStatus = 1;
    
    
    
}

- (void)offLine:(BOOL)keepPush
{
    if(sipInitialized)
    {
        if(_enablePushNotification && !keepPush)
        {
            [self addPushSupportWithPortPBX:NO];
        }
        
        [portSIPSDK unRegisterServer];
        
        [NSThread sleepForTimeInterval:1.0];
        [portSIPSDK unInitialize];
        sipInitialized = NO;
    }
    
    //if([_activityIndicator isAnimating])
    //[_activityIndicator stopAnimating];
    
    sipRegistrationStatus = 0;
}

- (void) refreshRegister
{
    if(sipRegistrationStatus == 0){
        //Not Register
        
        return;
    }
    else if(sipRegistrationStatus == 1){
        //is registering
        return;
    }else if(sipRegistrationStatus == 2){
        //has registered, refreshRegistration
        [portSIPSDK refreshRegistration:0];
        // [_labelDebugInfo setText:@"Refresh Registration..."];
        NSLog(@"Refresh Registration...");
    }else if(sipRegistrationStatus == 3){
        NSLog(@"retry a new register");
        //Register Failure
        [portSIPSDK unRegisterServer];
        [portSIPSDK unInitialize];
        sipInitialized = NO;
        [self Register];
    }
    
}

- (void) unRegister
{
    if(sipRegistrationStatus == 1||sipRegistrationStatus == 2){
        [portSIPSDK unRegisterServer];
        
        //[_labelDebugInfo setText:@"unRegister when background"];
        NSLog(@"unRegister when background");
        sipRegistrationStatus = 3;
        
        
    }
}

- (int)onRegisterSuccess:(int)statusCode withStatusText:(char*) statusText
{
  
    sipRegistrationStatus = 2;
    autoRegisterRetryTimes = 0;
    return 0;
    
}


- (int)onRegisterFailure:(int)statusCode withStatusText:(char*) statusText
{
    //[_viewStatus setBackgroundColor:[UIColor redColor]];
    
    //[_labelStatus setText:@"Not Connected"];
    
    //[_labelDebugInfo setText:[NSString stringWithFormat: @"onRegisterFailure: %s", statusText]];
    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Register Gagal"];
    NSLog(@"onRegisterFailure:%d %s",statusCode, statusText);
    
    //[_activityIndicator stopAnimating];
    sipRegistrationStatus = 3;
    if(statusCode != 401 && statusCode != 403  && statusCode != 404){
        
        //If the NetworkStatus not change, received onRegisterFailure event. can added a atuo reRegister Timer like this:
        // added a atuo reRegister Timer
        int interval = autoRegisterRetryTimes * 2 + 1;
        //max interval is 60
        interval = interval > 60?60:interval;
        autoRegisterRetryTimes ++;
        autoRegisterTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(refreshRegister) userInfo:nil repeats:NO];
    }
    return 0;
};



- (void)substract:(CDVInvokedUrlCommand*)command
{
    [self offLine:true];
    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"UnRegister"];
  
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}



@end
