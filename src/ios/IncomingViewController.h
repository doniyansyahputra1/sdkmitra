//
//  IncomingViewController.h
//  SIPSample
//
//  Created by Apple on 2/20/19.
//  Copyright © 2019 PortSIP. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IncomingViewController : UIViewController
{
    NSTimer *timer;
    NSTimer *timercall;
    NSString *idlocalize;
    int remainingCounts;
    int remainingCountsCall;
    
}
@property (weak, nonatomic) IBOutlet UIView *previewView;


@property (retain, nonatomic) AVCaptureSession *session;
@property (retain, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@property (weak, nonatomic) IBOutlet UILabel *ViewText;
@property (weak, nonatomic) IBOutlet UILabel *LabelIncomingA;
@property (weak, nonatomic) IBOutlet UILabel *LabelIncomingB;
@property (weak, nonatomic) IBOutlet UILabel *LabelTerima;
@property (weak, nonatomic) IBOutlet UILabel *LabelTolak;
@property (weak, nonatomic) IBOutlet UILabel *labelHeader;


@end

NS_ASSUME_NONNULL_END
