//
//  IncomingViewController.m
//  SIPSample
//
//  Created by Apple on 2/20/19.
//  Copyright © 2019 PortSIP. All rights reserved.
//

#import "IncomingViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface IncomingViewController ()

@end

@implementation IncomingViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self startCountdown];
    
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    idlocalize = [settings stringForKey:@"localize"];
    
    //[self CountdownCall];
    
    if ([idlocalize  isEqual: @"en"])
    {
        
        _LabelIncomingA.text = @"We would like verify your PermataBank product application.";
        _LabelIncomingB.text = @"Your availability to answer our call is highly appreciated.";
        _LabelTerima.text = @"Accept";
        _LabelTolak.text = @"Decline";
    }
    
    else
    {
        
        _LabelIncomingA.text = @"Kami ingin melakukan verifikasi pengajuan produk PermataBank.";
        _LabelIncomingB.text = @"Mohon kesediaan waktunya untuk menjawab.";
        _LabelTerima.text =  @"Terima";
        _LabelTolak.text = @"Tolak";
    }
    

    
}

- (void)camera
{
    
    self.session = [AVCaptureSession new];
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    //Add device
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    
    [self.session addInput:input];
    
    //Preview Layer
    AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer  layerWithSession:self.session];
    
    previewLayer.frame = _previewView.bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.previewView.layer addSublayer:previewLayer];
    
    //Start capture session
    [self.session startRunning];
    
    
    
}





- (IBAction)onAnsware:(id)sender {
    
    [self.session stopRunning];
    
//
//    AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
//    [appDelegate AnswareVideo];
    
    
}

- (IBAction)RejectCall:(id)sender
{
//    AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
//    [appDelegate Reject];
    [self dismissViewControllerAnimated:YES completion:nil];
 
}

- (void)startCountdown
{
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                             target:self
                                           selector:@selector(countDown)
                                           userInfo:nil
                                            repeats:YES];
    remainingCounts = 4;
}

-(void)countDown {
    --remainingCounts;
    if (remainingCounts == 0)
    {
        if ([idlocalize  isEqual: @"en"])
        {
            _ViewText.text = @"Incoming video call";
        }
        
        else
        {
            _ViewText.text = @"Panggilan video masuk";
        }
        remainingCounts = 4;
        
    }
    
    
    if (remainingCounts == 3)
    {
        if ([idlocalize  isEqual: @"en"])
        {
            _ViewText.text = @"Incoming video call.";
            
        }
        
        else
        {
            _ViewText.text = @"Panggilan video masuk.";
            
        }
    }
    if (remainingCounts == 2)
    {
        if ([idlocalize  isEqual: @"en"])
        {
            _ViewText.text = @"Incoming video call..";
        }
        
        else
        {
            _ViewText.text = @"Panggilan video masuk..";
        }
    }
    
    if (remainingCounts == 1)
    {
        if ([idlocalize  isEqual: @"en"])
        {
            _ViewText.text = @"Incoming video call...";
        }
        
        else
        {
            _ViewText.text = @"Panggilan video masuk...";
        }
    }
    
}


@end
