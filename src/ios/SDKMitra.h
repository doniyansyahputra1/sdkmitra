//
//  SDKMitra.h
//  MyApp
//
//  Created by mitra on 20/08/19.
//

#import <Cordova/CDV.h>
#import <Foundation/Foundation.h>
#import "HSSession.h"
#import "CallMananger.h"
#import "PortCxProvide.h"
#import <PushKit/PushKit.h>
#import "SoundService.h"
#import "Reachability.h"
#import <Intents/Intents.h>

@interface SDKMitra : CDVPlugin{}

- (void)add:(CDVInvokedUrlCommand*)command;
- (void)substract:(CDVInvokedUrlCommand*)command;
@end

@interface SDKMitra ()<UIApplicationDelegate,PKPushRegistryDelegate,UIAlertViewDelegate,CallManagerDelegate,PortSIPEventDelegate>
{
    PortSIPSDK *portSIPSDK;
@public
    NSString* UserName;
    NSString* DisplayName;
    NSString* AuthName;
    NSString* Password;
    NSString* UserDomain;
    NSString* SIPServer;
    NSString* SIPPort;
    
@protected
    
    BOOL    sipInitialized;
    int     sipRegistrationStatus;//0 - Not Register 1 - Registering 2 - Registered 3 - Register Failure/has been unregister
    BOOL    startByVoIPPush;
    
    NSTimer *autoRegisterTimer;
    int   autoRegisterRetryTimes;
    NSArray *transPortItems;
    NSArray *srtpItems;
    
}

+(SDKMitra*) sharedInstance;

@property NSInteger activeLine;
@property (nonatomic, retain) NSString *sipURL;
@property (nonatomic, assign) BOOL isConference;
@property (nonatomic, assign) BOOL  enablePushNotification;
@property (nonatomic, assign) BOOL  enableForceBackground;

@property (nonatomic, retain) PortCxProvider* cxProvide;
@property (nonatomic, retain) CallManager* callManager;

- (void) pressNumpadButton:(char )dtmf;
- (long) makeCall:(NSString*) callee videoCall:(BOOL)videoCall;
- (void) hungUpCall;
- (void) holdCall;
- (void) unholdCall;
- (void) referCall:(NSString*)referTo;
- (void) muteCall:(BOOL)mute;
- (void) setLoudspeakerStatus:(BOOL)enable;
- (void) getStatistics;
- (void) updateCall;
- (void) makeTest;
- (void) attendedRefer:(NSString*)referTo;
- (void) switchSessionLine;
- (void)addPushSupportWithPortPBX:(BOOL)enablePush;
- (void) refreshPushStatusToSipServer:(BOOL)addPushHeader;


- (BOOL)createConference:(PortSIPVideoRenderView *)conferenceVideoWindow;
- (void)setConferenceVideoWindow:(PortSIPVideoRenderView*)conferenceVideoWindow;
//- (void)removeFromConference:(long)sessionId;
//- (BOOL)joinToConference:(long)sessionId;
- (void)destoryConference:(UIView *)viewRemoteVideo;
- (void)AnswareVideo;
- (void)Reject;
- (void)RejectAuto;
- (void)RejectPermission;
- (void)StopRing;


- (void) refreshRegister;
- (void) unRegister;


- (int)onRegisterSuccess:(int)statusCode withStatusText:(char*) statusText;
- (int)onRegisterFailure:(int)statusCode withStatusText:(char*) statusText;

@end
